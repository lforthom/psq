#!/usr/bin/env python
"""
DD4hep simulation with some argument parsing
Based on M. Frank and F. Gaede runSim.py
   @author  A.Sailer
   @version 0.1

Modified with standard EIC EPIC requirements.
"""
from __future__ import absolute_import, unicode_literals
import logging
import sys
import DDG4
from DDG4 import OutputLevel as Output
from g4units import GeV, cm

import argparse
parser = argparse.ArgumentParser(
     prog='run_detector_simulation',
     description='''This runs the simulation  the but that is okay''',
     epilog='''
     This program should be run in the "compact" directory.
         ''')
parser.add_argument("-v","--verbose", help="increase output verbosity", type=int, default=0)
parser.add_argument("--compact", help="compact detector file",default="../compact/psQ.xml")
parser.add_argument("--vis", help="vis true/false", action="store_true",default=False)
parser.add_argument("--ui", help="ui setting tcsh or qt; default=qt", type=str,default="qt",dest="ui")
parser.add_argument("-b","--batch", help="batch turns off vis/ui", action="store_true",default=False, dest="batch")
parser.add_argument("-n","--nevents", help="number of events", type=int, default=0)
parser.add_argument("-m","--macro", help="macro to execute", default="vis.mac")
parser.add_argument("-o","--output", help="output file", default=None)
parser.add_argument("-i","--input", help="input data file", default=None)

args = parser.parse_args()

def run():
    logging.basicConfig(format='%(name)-16s %(levelname)s %(message)s', level=logging.INFO, stream=sys.stdout)
    logger = logging.getLogger('DDSim')

    kernel = DDG4.Kernel()
    kernel.loadGeometry('file:' + args.compact)
    DDG4.importConstants(kernel.detectorDescription(), debug=False)
    geant4 = DDG4.Geant4(kernel, tracker='Geant4TrackerCombineAction')

    if args.vis:
        geant4.printDetectors()

    geant4.setupUI(typ=args.ui, vis=args.vis, ui=not args.batch, macro=args.macro)

    kernel.NumEvents = args.nevents

    geant4.setupTrackingField(prt=True)

    prt = DDG4.EventAction(kernel, 'Geant4ParticlePrint/ParticlePrint')
    prt.OutputLevel = Output.DEBUG
    prt.OutputType = 3  # Print both: table and tree
    kernel.eventAction().adopt(prt)

    seq, act = geant4.addDetectorConstruction("Geant4DetectorGeometryConstruction/ConstructGeo")
    act.DebugMaterials = True
    act.DebugElements = False
    act.DebugVolumes = True
    act.DebugShapes = True
    act.DebugSurfaces = True
    #geant4.activeDetectors()
    geant4.printDetectors()

    evt_root = geant4.setupROOTOutput('RootOutput','test', mc_truth=True)

    xi = 0.1
    gun = geant4.setupGun("Gun",
        particle='proton',
        energy=(1. - xi) * 6500. * GeV,
        isotrop=False,
        multiplicity=1,
        position=(-10.*cm, 0., 0.),
        direction=(1, 0, 0)
    )
    gun.OutputLevel = Output.INFO

    geant4.setupTracker('psQ')

    # Now build the physics list:
    phys = geant4.setupPhysics('QGSP_BERT')
    ph = DDG4.PhysicsList(kernel, 'Geant4OpticalPhotonPhysics/OpticalGammaPhys')
    ph.VerboseLevel = 2
    ph.addParticleGroup('G4BosonConstructor')
    ph.addParticleGroup('G4LeptonConstructor')
    ph.addParticleGroup('G4MesonConstructor')
    ph.addParticleGroup('G4BaryonConstructor')
    ph.addParticleGroup('G4IonConstructor')
    ph.addParticleConstructor('G4OpticalPhoton')

    ph.addDiscreteParticleProcess('gamma', 'G4GammaConversion')
    ph.addDiscreteParticleProcess('gamma', 'G4ComptonScattering')
    ph.addDiscreteParticleProcess('gamma', 'G4PhotoElectricEffect')
    ph.addParticleProcess(str('e[+-]'), str('G4eMultipleScattering'), -1, 1, 1)
    ph.addParticleProcess(str('e[+-]'), str('G4eIonisation'), -1, 2, 2)
    ph.addParticleProcess(str('e[+-]'), str('G4eBremsstrahlung'), -1, 3, 3)
    ph.addParticleProcess(str('e+'), str('G4eplusAnnihilation'), 0, -1, 4)
    ph.addParticleProcess(str('mu[+-]'), str('G4MuMultipleScattering'), -1, 1, 1)
    ph.addParticleProcess(str('mu[+-]'), str('G4MuIonisation'), -1, 2, 2)
    ph.addParticleProcess(str('mu[+-]'), str('G4MuBremsstrahlung'), -1, 3, 3)
    ph.addParticleProcess(str('mu[+-]'), str('G4MuPairProduction'), -1, 4, 4)
    ph.enableUI()
    phys.adopt(ph)

    ph = DDG4.PhysicsList(kernel, 'Geant4ScintillationPhysics/ScintillatorPhys')
    ph.ScintillationYieldFactor = 1.0
    ph.ScintillationExcitationRatio = 1.0
    ph.TrackSecondariesFirst = False
    ph.VerboseLevel = 0
    ph.enableUI()
    phys.adopt(ph)

    ph = DDG4.PhysicsList(kernel, 'Geant4CerenkovPhysics/CerenkovPhys')
    ph.MaxNumPhotonsPerStep = 10
    ph.MaxBetaChangePerStep = 10.0
    ph.TrackSecondariesFirst = True
    ph.VerboseLevel = 2
    ph.enableUI()
    phys.adopt(ph)

    phys.dump()

    kernel.configure()
    kernel.initialize()
    kernel.run()
    kernel.terminate()

    #geant4.execute()

if __name__ == '__main__':
    run()
