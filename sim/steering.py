"""
DD4hep simulation with some argument parsing
Based on M. Frank and F. Gaede runSim.py
   @author  A.Sailer
   @version 0.1

Modified with settings for psQ simulation
"""
from __future__ import absolute_import, unicode_literals
import logging
import sys
import os

from DDSim.DD4hepSimulation import DD4hepSimulation
from g4units import GeV, cm

import ROOT

if __name__ == "__main__":
    logging.basicConfig(
        format="%(name)-16s %(levelname)s %(message)s",
        level=logging.INFO,
        stream=sys.stdout,
        )
    logger = logging.getLogger("DDSim")

    SIM = DD4hepSimulation()

    # Ensure that Cerenkov and optical physics are always loaded
    def setupCerenkov(kernel):
        from DDG4 import PhysicsList

        seq = kernel.physicsList()

        ph = PhysicsList(kernel, "Geant4OpticalPhotonPhysics/OpticalGammaPhys")
        ph.addParticleConstructor("G4OpticalPhoton")
        ph.VerboseLevel = 0
        ph.BoundaryInvokeSD = True
        ph.enableUI()
        seq.adopt(ph)

        cerenkov = PhysicsList(kernel, "Geant4CerenkovPhysics/CerenkovPhys")
        cerenkov.MaxNumPhotonsPerStep = 10
        cerenkov.MaxBetaChangePerStep = 10.0
        cerenkov.TrackSecondariesFirst = True
        cerenkov.VerboseLevel = 0
        cerenkov.enableUI()
        seq.adopt(cerenkov)

        seq.dump()
        return None

    SIM.physics.setupUserPhysics(setupCerenkov)

    # Allow energy depositions to 0 energy in trackers (which include optical detectors)
    #SIM.filter.tracker = "edep0"
    SIM.filter.tracker = "opticalphotons"

    # Some detectors are only sensitive to optical photons
    SIM.filter.filters["opticalphotons"] = dict(
        name="ParticleSelectFilter/OpticalPhotonSelector",
        parameter={"particle": "opticalphoton"},
    )
    SIM.filter.mapDetFilter["psQ"] = "opticalphotons"

    # Use the optical tracker for the PFRICH
    SIM.action.mapActions["psQ"] = "Geant4OpticalTrackerAction"

    # Disable user tracker particle handler, so hits can be associated to photons
    SIM.part.userParticleHandler = ""

    # Particle gun settings
    #SIM.numberOfEvents = 1000
    SIM.numberOfEvents = 1
    SIM.enableGun = True
    xi = 0.1
    if True:
        SIM.gun.energy = (1. - xi) * 6800. * GeV
        #SIM.gun.energy = 100. * GeV  #FIXME
        SIM.gun.particle = "proton"
        SIM.gun.position = (-3. * cm, 0., 0.)
    else:
        SIM.gun.energy = 4.e-9 * GeV
        SIM.gun.particle = "opticalphoton"
        SIM.gun.position = (0., 0., 0.)
    SIM.gun.multiplicity = 1
    #SIM.gun.position = (-2. * cm, (4./2 - 0.5) * cm, 0.)
    #SIM.gun.position = (-2. * cm, (4./2 - 0.05) * cm, 0.)
    #SIM.gun.position = (-2. * cm, (4./2 - 0.05) * cm, -(4./2 - 0.05) * cm)
    SIM.gun.direction = (1, 0, 0)

    # Default is enable visualization of tracks
    SIM.runType = "qt"
    # Default compact file
    SIM.compactFile = "../compact/psQ.xml"
    #SIM.macroFile ='vis_bdsim.mac'
    SIM.macroFile ='vis.mac'
    # Output file (assuming CWD)
    SIM.outputFile = "psq_sim.root"

    # Override with user options
    SIM.parseOptions()

    # Run the simulation
    try:
        SIM.run()
        logger.info("TEST: passed")

    except NameError as e:
        logger.fatal("TEST: failed")
