/*
 *  psQsim: a DD4hep-based tool for the simulation of the psQ detector
 *  Copyright (C) 2023  Laurent Forthomme
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Detector.h"
#include "DD4hep/OpticalSurfaces.h"
#include "DDRec/DetectorData.h"
#include "DDRec/Surface.h"

static dd4hep::Ref_t create_detector(dd4hep::Detector& description, xml_h e, dd4hep::SensitiveDetector sens) {
  const xml_det_t param_det{e};
  const xml_det_t x_rp = param_det.child(_Unicode(rp)), x_quartz = param_det.child(_Unicode(quartz)),
                  x_pmt = param_det.child(_Unicode(pmt)),
                  x_scoring_plane_wall = param_det.child(_Unicode(scoring_plane_wall)),
                  x_scoring_plane_pmt = param_det.child(_Unicode(scoring_plane_pmt));
  const auto det_name = param_det.nameStr();
  const auto det_id = param_det.id();
  sens.setType("tracker");

  // retrieve all user-steerable parameters
  const double quartz_thickness = dd4hep::_toDouble("quartz_thickness"),
               quartz_paint_thickness = dd4hep::_toDouble("quartz_paint_thickness"),
               quartz_width = dd4hep::_toDouble("quartz_width"), quartz_length = dd4hep::_toDouble("quartz_length"),
               quartz_angle = dd4hep::_toDouble("quartz_angle");
  //----> construct coordinates set for the diamond-shaped crystal
  const auto generateRhomboidCoords = [](double thickness, double length, double angle) -> std::vector<double> {
    const double dx = 0.5 * thickness * tan(angle), y1 = -0.5 * thickness, y2 = 0.5 * thickness;
    const double x1y1 = -0.5 * length - dx, x2y1 = 0.5 * length - dx, x1y2 = -0.5 * length + dx,
                 x2y2 = 0.5 * length + dx;
    return std::vector<double>{x1y1, y1, x1y2, y2, x2y2, y2, x2y1, y1, x1y1, y1, x1y2, y2, x2y2, y2, x2y1, y1};
  };
  auto quartz_coords = generateRhomboidCoords(quartz_thickness, quartz_length, quartz_angle);

  const double pmt_x = dd4hep::_toDouble("pmt_x"), pmt_y = dd4hep::_toDouble("pmt_y"),
               pmt_z = dd4hep::_toDouble("pmt_z");
  const double pmt_distance = dd4hep::_toDouble("pmt_distance");
  const double world_x = dd4hep::_toDouble("world_x"), world_y = dd4hep::_toDouble("world_y"),
               world_z = dd4hep::_toDouble("world_z");
  const double wall_scoring_plane_distance = dd4hep::_toDouble("wall_scoring_plane_distance");
  const double pmt_scoring_plane_yz = dd4hep::_toDouble("pmt_scoring_plane_yz");
  const bool use_paint = dd4hep::_toInt("use_paint");
  const bool use_5faces_paint = dd4hep::_toInt("use_5faces_paint");
  const bool use_pmt_scoring_plane = dd4hep::_toInt("use_pmt_scoring_plane");
  std::cout << "paint:" << use_paint << std::endl;

  // construction of all volumes
  dd4hep::Box rp_box(world_x, world_y, world_z);
  dd4hep::Volume rp_vol("RP", rp_box, description.material(x_rp.attr<std::string>(_Unicode(material))));
  dd4hep::EightPointSolid quartz("Quartz", quartz_width / 2., quartz_coords.data());
  dd4hep::Volume quartz_vol("Quartz_vol", quartz, description.material(x_quartz.attr<std::string>(_Unicode(material))));
  dd4hep::Box pmt(pmt_x, pmt_y, pmt_z);
  dd4hep::Volume pmt_vol("PMT", pmt, description.material(x_pmt.attr<std::string>(_Unicode(material))));
  dd4hep::Box scoring_plane_wall(10. * dd4hep::um, pmt_scoring_plane_yz, pmt_scoring_plane_yz);
  dd4hep::Volume scoring_plane_wall_vol(
      "ScoringPlaneWall",
      scoring_plane_wall,
      description.material(x_scoring_plane_wall.attr<std::string>(_Unicode(material))));
  dd4hep::Box scoring_plane_pmt(pmt_scoring_plane_yz, 10. * dd4hep::um, pmt_scoring_plane_yz);
  dd4hep::Volume scoring_plane_pmt_vol("ScoringPlanePMT",
                                       scoring_plane_pmt,
                                       description.material(x_scoring_plane_pmt.attr<std::string>(_Unicode(material))));
  /*if (!use_pmt_scoring_plane) {
    dd4hep::rec::Vector3D u(1, 0, 0), v(0, 1, 0), n(0, 0, 1);
    dd4hep::rec::VolPlane scoring_plane(scoring_plane_vol,
                                        dd4hep::rec::SurfaceType::Sensitive,
                                        world_z,
                                        world_z,
                                        v,
                                        n,
                                        u,
                                        dd4hep::rec::Vector3D(world_x, 0., 0.));
  }*/
  //surf.setSensitiveDetector(sens);

  // visualisation options
  rp_vol.setVisAttributes(description, x_rp.visStr());
  quartz_vol.setVisAttributes(description, x_quartz.visStr());
  pmt_vol.setVisAttributes(description, x_pmt.visStr());
  pmt_vol.setSensitiveDetector(sens);
  if (!use_pmt_scoring_plane) {
    scoring_plane_wall_vol.setVisAttributes(description, x_scoring_plane_wall.visStr());
    scoring_plane_wall_vol.setSensitiveDetector(sens);
  }
  scoring_plane_pmt_vol.setVisAttributes(description, x_scoring_plane_pmt.visStr());
  scoring_plane_pmt_vol.setSensitiveDetector(sens);

  // volumes placement
  dd4hep::DetElement sdet(det_name, param_det.id());
  auto quartz_place = rp_vol.placeVolume(quartz_vol);
  //quartz_place.addPhysVolID("quartz", 1);
  //quartz_place.addPhysVolID("system", det_id).addPhysVolID("rp", 1).addPhysVolID("quartz", 1);
  auto quartz_detelem = dd4hep::DetElement(sdet, "quartz_de", 0);
  quartz_detelem.setPlacement(quartz_place);

  auto quartz_paint_detelem = dd4hep::DetElement(sdet, "paint_de", 0);

  if (!use_pmt_scoring_plane) {
    auto scoring_plane_wall_place =
        rp_vol.placeVolume(scoring_plane_wall_vol, dd4hep::Position(wall_scoring_plane_distance, 0., 0.));
    scoring_plane_wall_place.addPhysVolID("system", det_id).addPhysVolID("rp", 1).addPhysVolID("detector", 2);
  }

  const auto position_distance_angle = [&quartz_length](double distance, double angle, double yoffset = 0.) {
    return dd4hep::Transform3D(dd4hep::RotationZ(0.5 * M_PI - angle),
                               dd4hep::Position(quartz_length / 2. + distance * cos(angle) - yoffset * tan(angle),
                                                0. - distance * sin(angle) - yoffset,
                                                0.));
  };

  if (use_pmt_scoring_plane) {
    auto scoring_plane_pmt_place =
        rp_vol.placeVolume(scoring_plane_pmt_vol,
                           position_distance_angle(pmt_distance, quartz_angle, -quartz_thickness / 2.
                                                   /*+ pmt_scoring_plane_yz * cos(quartz_angle)*/));
    scoring_plane_pmt_place.addPhysVolID("system", det_id).addPhysVolID("rp", 1).addPhysVolID("detector", 4);
    //dd4hep::rec::volSurfaceList(sdet)->push_back(scoring_plane_pmt);
  } else {
    auto pmt_place = rp_vol.placeVolume(pmt_vol, position_distance_angle(pmt_distance, quartz_angle));
    pmt_place.addPhysVolID("system", det_id).addPhysVolID("rp", 1).addPhysVolID("detector", 3);
  }

  auto rp_place = description.pickMotherVolume(sdet).placeVolume(rp_vol);
  //rp_place.addPhysVolID("system", det_id).addPhysVolID("rp", 1).addPhysVolID("detector", 0);
  sdet.setPlacement(rp_place);

  // description of all optical surfaces
  auto surf_mgr = description.surfaceManager();

  //auto quartz_surf_border = dd4hep::SkinSurface(description, sdet, "QuartzAir", quartz_surf, quartz_vol);
  //quartz_surf_border.isValid();

  auto quartz_surf = surf_mgr.opticalSurface(x_quartz.attr<std::string>(_Unicode(surface)));
  if (use_paint) {
    const xml_det_t x_paint = param_det.child(_Unicode(paint));
    auto paint_surf = surf_mgr.opticalSurface(x_paint.attr<std::string>(_Unicode(surface)));
    const auto dx = 0.5 * quartz_thickness * tan(quartz_angle);
    if (use_5faces_paint) {
      dd4hep::Box quartz_paint_long_topbot(quartz_length / 2., quartz_paint_thickness / 2., quartz_width / 2.);
      dd4hep::EightPointSolid quartz_paint_long_lr(
          "Quartz_paint_side", quartz_paint_thickness / 2., quartz_coords.data());
      dd4hep::Box quartz_paint_trans(
          quartz_paint_thickness / 2., quartz_thickness / cos(quartz_angle) / 2., quartz_width / 2.);

      const auto dz = (quartz_width + quartz_paint_thickness) / 2.;  // horizontal offset
      dd4hep::UnionSolid quartz_paint1(quartz_paint_long_lr,
                                       quartz_paint_long_topbot,
                                       dd4hep::Position(dx, (quartz_thickness + quartz_paint_thickness) / 2., dz));
      dd4hep::UnionSolid quartz_paint2(quartz_paint1,
                                       quartz_paint_long_topbot,
                                       dd4hep::Position(-dx, -(quartz_thickness + quartz_paint_thickness) / 2., dz));
      dd4hep::UnionSolid quartz_paint3(quartz_paint2, quartz_paint_long_lr, dd4hep::Position(0., 0., 2. * dz));
      dd4hep::UnionSolid quartz_paint(
          quartz_paint3,
          quartz_paint_trans,
          dd4hep::Transform3D(dd4hep::RotationZ(M_PI - quartz_angle),
                              dd4hep::Position(-(quartz_length + quartz_paint_thickness) / 2., 0., dz)));
      dd4hep::Volume quartz_paint_vol(
          "Quartz_paint", quartz_paint, description.material(x_paint.attr<std::string>(_Unicode(material))));

      auto quartz_paint_place = rp_vol.placeVolume(quartz_paint_vol, dd4hep::Position(0., 0., -dz));

      quartz_paint_detelem.setPlacement(quartz_paint_place);
      quartz_paint_vol.setVisAttributes(description, x_paint.visStr());

      auto quartz_paint_surf_border =
          dd4hep::BorderSurface(description, sdet, "OpaqueAir", paint_surf, quartz_place, quartz_paint_place);
      quartz_paint_surf_border.isValid();
      auto opaque_surf_border = dd4hep::SkinSurface(description, sdet, "OpaquePaintAir", paint_surf, quartz_paint_vol);
      opaque_surf_border.isValid();
      auto quartz_unpainted_surf_border =
          dd4hep::BorderSurface(description, sdet, "QuartzAir", quartz_surf, quartz_place, rp_place);
      quartz_unpainted_surf_border.isValid();
    } else {
      dd4hep::Box quartz_paint(quartz_length / 2., quartz_paint_thickness / 2., quartz_width / 2.);
      dd4hep::Volume quartz_paint_vol(
          "Quartz_paint", quartz_paint, description.material(x_paint.attr<std::string>(_Unicode(material))));
      auto quartz_paint_place = rp_vol.placeVolume(
          quartz_paint_vol, dd4hep::Position(dx, (quartz_thickness + quartz_paint_thickness) / 2., 0.));
      quartz_paint_detelem.setPlacement(quartz_paint_place);
      quartz_paint_vol.setVisAttributes(description, x_paint.visStr());

      //auto opaque_surf_border = dd4hep::SkinSurface(description, sdet, "OpaqueAir", paint_surf, quartz_paint_place);
      auto opaque_surf_border = dd4hep::SkinSurface(description, sdet, "OpaqueAir", paint_surf, quartz_paint_vol);
      opaque_surf_border.isValid();
    }
  }

  return sdet;
}
DECLARE_DETELEMENT(DD4hep_psQ, create_detector)
